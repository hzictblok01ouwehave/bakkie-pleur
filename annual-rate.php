<?php
require_once 'includes/globals.php';
require_once 'includes/requireSession.php';
require_once 'includes/requirePenningmeester.php';
require_once 'includes/functions.php';
require_once 'includes/connectdb.php';
?>
<!DOCTYPE html>
<html lang="nl">

	<head>
		<?php

		include_once 'includes/head.php';
		?>

		<title><?php echo SITE_TITLE; ?>
			- Jaar tarief</title>

	</head>

	<body>

		<?php
		include_once 'includes/wrapper.php';
		?>

		<!-- Sidebar -->
		<?php

		include_once 'includes/sidebar.php';
		?>
		<!-- /#sidebar-wrapper -->

		<!-- Page Content -->
		<div id="page-content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-header">
							<h1>Jaar tarief <small>Bekijken</small></h1>
						</div>
						<p>
							Op deze pagina kunt u het jaar tarief bekijken.
						</p>
						<ul class="nav nav-tabs">

							<!--<li role="presentation" class="active"></li>
							<a href="annual-rate.php">Jaar tarief</a>
-->
							<li role="presentation"  >
								<a href="annual-rate2.php">Jaar tarief toevoegen</a>
							</li>
						
						</ul>
						<div class="table-responsive">
							<table class="table table-striped table-hover" id="cashbookEntriesTable">
								<thead>
									<tr>
										<th>ID</th>
										<th>Jaar</th>
										<th>Contributie</th>
										<th>Toeslag water en elektriciteit </th>
										<th>Liggeld boot 1</th>
										<th>Liggeld boot 2</th>
										<th>Betaald </th>
										<th>Totaal </th>
									</tr>
								</thead>
								<tbody id="invoicesEntries2">
									<?php
									$query = "SELECT 	oh_annual_rate2.ID AS Naam,
														oh_annual_rate2.Datum AS Datum,
														oh_annual_rate2.contributie AS contributie,
														oh_annual_rate2.toeslag AS toeslag,
														oh_annual_rate2.boot1 AS boot1,
														oh_annual_rate2.boot2 AS boot2,
														oh_annual_rate2.betaald AS betaald 
											FROM oh_annual_rate2
											";
									$invoices = $dataManager -> rawQuery($query);
									
									foreach ($invoices as $invoice) {
										echo '<tr>';
										echo '<td>' . $invoice["Naam"] . '</td>';
										echo '<td>' . $invoice["Datum"] . '</td>';
										echo '<td>' . $invoice["contributie"] . '</td>';
										echo '<td>' . $invoice["toeslag"] . '</td>';
										echo '<td>' . $invoice["boot1"] . '</td>';
										echo '<td>' . $invoice["boot2"] . '</td>';
										echo '<td>' . (($invoice["betaald"] == 0) ? 'Nee' : 'Ja') . '</td>';
										echo '<td>' . ($invoice["contributie"]+$invoice["toeslag"]+$invoice["boot1"]+$invoice["boot2"]) . '</td>';
										echo '</tr>';
									}
									?>
								</tbody>
							</table>
						</div>
						<div class="text-center" id="page-selection">
							<ul id="pagination"></ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- /#page-content-wrapper -->

		<!-- /#wrapper -->

		<!-- Footer -->
		<script src="js/invoices-pagination2.js"></script>
		<?php

		include_once 'includes/footer.php';
		?>

	</body>

</html>