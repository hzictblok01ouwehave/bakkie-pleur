<?php
    require_once 'includes/globals.php';
    require_once 'includes/requireSession.php';
    require_once 'includes/functions.php';
    require_once 'includes/connectdb.php';
?>
<!DOCTYPE html>
<html lang="nl">

<head>
    <?php

        include_once 'includes/head.php';

    ?>

    <title><?php echo SITE_TITLE; ?> - Schepen - Mijn schepen</title>

</head>

<body>

    <?php include_once 'includes/wrapper.php'; ?>

        <!-- Sidebar -->
        <?php

            include_once 'includes/sidebar.php';

        ?>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-header">
                            <h1>Schepen <small></small></h1>
                        </div>
                        <p>Op deze pagina kunt u uw schepen beheren.</p>

                        <ul class="nav nav-tabs">
                            <li role="presentation" class="active"><a href="ships-penningmeester.php">Overzicht schepen</a></li>
                            <li role="presentation"><a href="ships-add.php">Schip toevoegen</a></li>
                            <li role="presentation"><a href="ships-remove.php">Schip verwijderen</a></li>
                            <li role="presentation"><a href="ships-search-penningmeester.php">Zoek schip</a></li>
                        </ul>

                        <div class="table-responsive">
                            <table class="table table-striped table-hover">

                                <thead>
                                    <tr>
                                        <th>Naam</th>
                                        <th>Lengte (m)</th>
                                        <th>Aanpassen</th>
                                        <th>Details</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
										
                                        $oh_ships = $dataManager->get('oh_ships');

                                        foreach($oh_ships as $oh_ship) {
                                            echo '<tr>';
                                                echo '<td>' . $oh_ship["Naam"] . '</td>';
                                                echo '<td>' . round($oh_ship["Lengte"], 2) . '</td>';
                                                echo '<td><a href="ships-edit.php?id=' . $oh_ship["ID"] . '"><i class="fa fa-cog"></i></a></td>';
                                                echo '<td><a href="ships-details.php?id=' . $oh_ship["ID"] . '"><i class="fa fa-arrow-right"></i></a></td>';
                                            echo '</tr>';
                                        }

                                    ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Footer -->
    <?php

        include_once 'includes/footer.php';

    ?>

</body>

</html>