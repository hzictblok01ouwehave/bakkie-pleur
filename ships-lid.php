<?php
require_once 'includes/globals.php';
require_once 'includes/requireSession.php';
require_once 'includes/functions.php';
require_once 'includes/connectdb.php';
?>
<!DOCTYPE html>
<html lang="nl">

	<head>
		<?php

		include_once 'includes/head.php';
		?>

		<title><?php echo SITE_TITLE; ?> - Schepen - Mijn schepen</title>

	</head>

	<body>

		<?php
		include_once 'includes/wrapper.php';
		?>

		<!-- Sidebar -->
		<?php

		include_once 'includes/sidebar.php';
		?>
		<!-- /#sidebar-wrapper -->

		<!-- Page Content -->
		<div id="page-content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-header">
							
								<?php
								 $ID = $_SESSION['ID'];
								 if (!isset($_SESSION['ID'])){
									echo '<h1>Schepen</h1>'; 
									echo 'Geen data';
								}
								else{
								echo '<h1>Schepen <small>';
								$oh_memberss = $dataManager -> rawQuery("SELECT Voornaam, Achternaam,Tussenvoegsel FROM oh_members WHERE User_ID = $ID");
								print_r($oh_memberss[0]['Voornaam']);
								echo " ";
								print_r($oh_memberss[0]['Tussenvoegsel']);
								print_r($oh_memberss[0]['Achternaam']);
							 ?>
							 </small> </h1>
						</div>

						<!--<ul class="nav nav-tabs">
						<li role="presentation" class="active"><a href="ships.php">Schepen overzicht</a></li>!
						</ul>!-->

						<div class="table-responsive">
							<table class="table table-striped table-hover">

								<thead>
									<tr>
										<th>Naam</th>
										<th>Lengte (m)</th>
										<th>Aanpassen</th>
										<th>Details</th>
									</tr>
								</thead>

								<tbody>
									<?php
									 
									if (isset($_GET['id'])) {
										$member_id = $_GET['id'];
									} else {
										$member_id = $_SESSION['Member_ID'];
									}
									
									$oh_ships = $dataManager -> rawQuery("SELECT Ship_ID FROM oh_member_ship WHERE Member_ID = $member_id");
									foreach ($oh_ships as $oh_ship) {
										$oh_ships = $dataManager -> rawQuery("SELECT * FROM oh_ships WHERE ID = '" . $oh_ship["Ship_ID"] . "'");
										foreach ($oh_ships as $oh_ship) {
											echo '<tr>';
											echo '<td>' . $oh_ship["Naam"] . '</td>';
											echo '<td>' . round($oh_ship["Lengte"], 2) . '</td>';
											echo '<td><a href="ships-edit-lid.php?id=' . $oh_ship["ID"] . '"><i class="fa fa-cog"></i></a></td>';
											echo '<td><a href="ships-details-lid.php?id=' . $oh_ship["ID"] . '"><i class="fa fa-arrow-right"></i></a></td>';
											echo '</tr>';
										}
										
									}
									}
									?>
								</tbody>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /#page-content-wrapper -->

		</div>
		<!-- /#wrapper -->

		<!-- Footer -->
		<?php

		include_once 'includes/footer.php';
		?>
	</body>

</html>