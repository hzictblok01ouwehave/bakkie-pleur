<?php
require_once 'includes/globals.php';
require_once 'includes/requireSession.php';
require_once 'includes/requireHavenmeester.php';
require_once 'includes/functions.php';
require_once 'includes/connectdb.php';
?>
<!DOCTYPE html>
<html lang="nl">

<head>
    <?php

        include_once 'includes/head.php';

    ?>

    <title><?php echo SITE_TITLE; ?> - Leden - Overzicht</title>

</head>

<body>

    <?php include_once 'includes/wrapper.php'; ?>

    <!-- Sidebar -->
    <?php

        include_once 'includes/sidebar.php';

    ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                        <h1>Leden <small>Overzicht</small></h1>
                    </div>
                    <p></p>


                    <hr/>

                    <div class="table-responsive">
                        <table class="table table-striped table-hover">

                            <thead>
                            <tr>
                                <th>Voornaam</th>
                                <th>Tussenvoegsel</th>
                                <th>Achternaam</th>
                                <th>Adres</th>
                                <th>Postcode</th>
                                <th>Woonplaats</th>
                                <th>Telefoonnummer</th>
                                <th>IBAN</th>
                                <th>Schepen</th>
                            </tr>
                            </thead>

                            <tbody id="member-content">
                               <?php
										
                                        $oh_members = $dataManager->get('oh_members');

                                        foreach($oh_members as $oh_members) {
                                        	
                                            echo '<tr>';
                                                echo '<td>' . $oh_members["Voornaam"] . '</td>';
                                                echo '<td>' . $oh_members["Tussenvoegsel"] .'</td>';
                                                echo '<td>' . $oh_members["Achternaam"] . '</td>';
                                                echo '<td>' . $oh_members["Adres"] . '</td>';
                                                echo '<td>' . $oh_members["Postcode"] . '</td>';
                                                echo '<td>' . $oh_members["Woonplaats"] . '</td>';
                                                echo '<td>' . $oh_members["Telefoonnummer"] . '</td>';
												echo '<td>' . $oh_members["IBAN"] . '</td>';
												echo '<td><a href="ships-lid.php?id=' . $oh_members["ID"] . '"><i class="fa fa-arrow-right"></i></a></td>';
												
												// post data:
												//naam=&minLengte=&maxLengte=&naamEigenaar=steef&page=1
                                        }

                                    ?>
                            </tbody>

                        </table>
                    </div>
                    <div class="text-center" id="page-selection">
                        <ul id="pagination"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Footer -->
    <script src="js/members-pagination.js"></script>
    <?php

        include_once 'includes/footer.php';

    ?>

</body>

</html>