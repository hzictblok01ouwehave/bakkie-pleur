<?php
require_once 'includes/globals.php';
require_once 'includes/requireSession.php';
require_once 'includes/requirePenningmeester.php';
require_once 'includes/functions.php';
require_once 'includes/connectdb.php';
?>
<!DOCTYPE html>
<html lang="nl">

	<head>
		<?php

		
		include_once 'includes/head.php';
		?>
		<script type="text/javascript">
						$(document).ready(function () {

			('#addInvoice #add').click(function () {
			var selectBox = '<tr> <td class="form-group"> <select class="form-control" name="invoiceLines[]" class="form-control"> <?php $categories = $dataManager -> get('oh_price_category');
				foreach ($categories as $category) { echo '<option value="' . $category['ID'] . '" id="Categorie_ID">' . $category['Naam'] . '</option>';
				}
			
			?>
				</select> </td> <td class="form-group"> <input type="number" class="form-control" name="invoiceAmounts[]"> </td> <td> <input type="text" class="form-control" name="invoicePrices[]"> </td> <td> <i class="fa fa-minus-circle remove-line"></i> </td> </tr>';
				selectBox = $(selectBox);

				selectBox.hide();
				$('#invoices tr:last').after(selectBox);

				selectBox.fadeIn('slow');
				return false;
				});

				$('#addInvoice').on('click', '.remove-line', function () {

				$(this).parent().parent().fadeOut("slow", function () {
				$(this).remove();
				$('.number').each(function (index) {
				$(this).text(index + 1);
				});
				});
				return false;
				});
				});
		</script>

		<title><?php echo SITE_TITLE; ?>
			- Jaartarief toevoegen</title>
	</head>

	<body>

		<?php
		include_once 'includes/wrapper.php';
		?>

		<!-- Sidebar -->
		<?php

		include_once 'includes/sidebar.php';
		?>
		<!-- /#sidebar-wrapper -->

		<!-- Page Content -->
		<div id="page-content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-header">
							<h1>Jaar tarief <small>Toevoegen</small></h1>
						</div>
						<p>
							Op deze pagina kunt u het jaartarief toevoegen.
						</p>
						<ul class="nav nav-tabs">
							<li role="presentation" >
								<a href="annual-rate.php">Jaartarief bekijken</a>
							</li>
						</ul>

						<?php
                if ($_SERVER ['REQUEST_METHOD'] == 'POST') {

                    $memberID = cleanInput($_POST['selectMember']);
                    $jaar = $_POST['jaar'];
                    $contributie = $_POST['contributie'];
                    $toeslag = $_POST['toeslag'];
					$boot1 = $_POST['boot1'];
					$boot2 = $_POST['boot2'];
					$betaald = $_POST['betaald'];
					$ID = $_SESSION['ID'];
					
					if (0 < 1) {
                        $data = array('Datum' => $jaar[0],
									'contributie' => $contributie[0],
									'toeslag' => $toeslag[0],
									'boot1' => $boot1[0],
									'boot2' => $boot2[0],
									'betaald' => $betaald[0],
									'ID' => $ID);
                        $insert = $dataManager->insert('oh_annual_rate2', $data);

                        if ($insert) {
                            echo '<div class="alert alert-success" role="alert">Jaartarief is succesvol toegevoegd!</div>';
                            echo '<p>Klik <a href="/annual-rate.php">hier</a> om naar de hoofdpagina te gaan.</p>';
                            echo "<p>Of klik <a href=" . $_SERVER ['REQUEST_URI'] . ">hier</a> om nog een jaartarief toe te voegen.";
                        } else {
                           echo '<div class="alert alert-danger" role="alert">Het lijkt er op alsof er een fout is met de verbinding van de database...</div>';
                           echo "<p>Klik <a href=" . $_SERVER ['REQUEST_URI'] . ">hier</a> om het opnieuw te proberen.</p>";
						}
                    } else {
                           echo '<div class="alert alert-danger" role="alert">Het lijkt er op alsof niet alle gegevens zijn ingevuld...</div>';
                           echo "<p>Klik <a href=" . $_SERVER ['REQUEST_URI'] . ">hier</a> om het opnieuw te proberen.</p>";
                    }
                } else {




						?>

						<form class="clearfix horizontalSearchForm" id="addInvoice" role="form"  method="POST" enctype="multipart/form-data">

							<div class="form-group col-md-5" id="selectMembers">
								<label for="selectMember">Selecteer lid</label>

								<select class="form-control" name="selectMember" id="selectMember">
									<?php
									$members = $dataManager -> get('oh_members');

									foreach ($members as $member) {
										$eigenaar = generateName($member['Voornaam'], $member['Tussenvoegsel'], $member['Achternaam']);

										echo '<option value="' . $member["ID"] . '">' . $eigenaar . '</option>';
									}
									?>
								</select>
							</div>

							<div class="form-group col-md-5">
								<label for="date">Jaar:</label>
								<input type="text" class="form-control" name="jaar[]">
							</div>

							<div class="table-responsive col-md-12">
								<table class="table table-striped" id="invoices">
									<thead>
										<tr>
											<th>Contributie</th>
											
											<th>Toeslag water en elektriciteit </th>
											<th>Liggeld boot 1</th>
											<th>Liggeld boot 2</th>
											<th>Betaald </th>
										</tr>										
									</thead>
									<tbody>
										<tr>
											
											<td class="form-group">
												<input type="text" class="form-control" name="contributie[]">
											</td>
											<td>
												<input type="text" class="form-control" name="toeslag[]">
											</td>
											<td>
												<input type="text" class="form-control" name="boot1[]">
											</td>
											<td>
												<input type="text" class="form-control" name="boot2[]">								
											</td>
											<td>
												<input type="text" class="form-control" name="betaald[]">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-1">
								<button type="submit" class="btn btn-primary ">
									Aanmaken
								</button>
							</div>

						</form>

						<?php
						}
						?>

						<hr/>

					</div>
				</div>
			</div>
		</div>

		<!-- /#page-content-wrapper -->

		<!-- /#wrapper -->

		<!-- Footer -->
		<?php

		include_once 'includes/footer.php';
		?>

	</body>

</html>
