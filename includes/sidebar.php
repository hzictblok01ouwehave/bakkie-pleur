<div id="sidebar-wrapper">
	<a class="btn btn-primary" id="menu-toggle"> <i class="fa fa-lg fa-angle-left"></i> </a>

	<a class="btn btn-primary" id="menu-toggle"> <i class="fa fa-lg fa-angle-left"></i> </a>

	<ul class="sidebar-nav">
		<li class="sidebar-brand">
			<a href="#"> De 'n Ouwe Haven </a>
		</li>
		<li>
			<a href="index.php">Dashboard</a>
		</li>
		<li>
			<a href="ships-lid.php"><i class="fa fa-ship"></i> Schepen</a>
		</li>
		<li>
			<a href="logout.php"><i class="fa fa-cog"></i> Uitloggen</a>
		</li>
		<?php
		$status = $_SESSION['user_status'];

		if ($status == '2') {
		echo '
		<li class="sidebarSub">
			<i class="fa fa-list-ul"></i> Havenmeester
			<ul>
				<li>
					<a href="ships.php"><i class="fa fa-ship"></i> Schepen</a>
				</li>
				<li>
					<a href="members.php"><i class="fa fa-users "></i> Leden</a>
				</li>
			</ul>
		</li>';
		
		} elseif ($status == '3') {
		echo '
		<li class="sidebarSub">
			<i class="fa fa-btc"></i> Penningmeester
			<ul>
				<li>
					<a href="ships-penningmeester.php"><i class="fa fa-ship"></i> Schepen</a>
				</li>
				<li>
					<a href="members-penningmeester.php"><i class="fa fa-users "></i> Leden</a>
				</li>
				<li>
					<a href="transactions.php"><i class="fa fa-file-text"></i> Transacties</a>
				</li>
				<li>
					<a href="categories.php"><i class="fa fa-list"></i> Rubrieken</a>
				</li>
				<li>
					<a href="balances.php"><i class="fa fa-euro"></i> Saldi</a>
				</li>
				<li>
					<a href="financial-year.php"><i class="fa fa-bar-chart"></i> Financieel jaarverslag</a>
				</li>
				<li>
					<a href="invoices.php"><i class="fa fa-file"></i> Facturen</a>
				</li>
				<li>
					<a href="cashbook.php"><i class="fa fa-money"></i> Kasboek</a>
				</li>
				<li>
					<a href="annual-rate.php"><i class="fa fa-money"></i> Jaar tarief</a>
				</li>
			</ul>
		</li>
';

		} elseif ($status == '4') {
		echo '
		<li class="sidebarSub">
			<i class="fa fa-list-ul"></i> Havenmeester
			<ul>
				<li>
					<a href="ships.php"><i class="fa fa-ship"></i> Schepen</a>
				</li>
				<li>
					<a href="members.php"><i class="fa fa-users "></i> Leden</a>
				</li>
				<li>
                    <a href="cashbook.php"><i class="fa fa-money"></i> Kasboek</a>
                </li>
			</ul>
		</li>
		<li class="sidebarSub">
			<i class="fa fa-btc"></i> Penningmeester
			<ul>
				<li>
					<a href="ships-penningmeester.php"><i class="fa fa-ship"></i> Schepen</a>
				</li>
				<li>
					<a href="transactions.php"><i class="fa fa-file-text"></i> Transacties</a>
				</li>
				<li>
					<a href="categories.php"><i class="fa fa-list"></i> Rubrieken</a>
				</li>
				<li>
					<a href="balances.php"><i class="fa fa-euro"></i> Saldi</a>
				</li>
				<li>
					<a href="financial-year.php"><i class="fa fa-bar-chart"></i> Financieel jaarverslag</a>
				</li>
				<li>
					<a href="invoices.php"><i class="fa fa-file"></i> Facturen</a>
				</li>
				<li>
					<a href="cashbook.php"><i class="fa fa-money"></i> Kasboek</a>
				</li>
				<li>
					<a href="annual-rate.php"><i class="fa fa-money"></i> Jaar tarief</a>
				</li>
			</ul>
		</li>
		<li class="sidebarSub">
			<i class="fa fa-gear"></i> Administrator
			<ul>
				<!-- Admin pagina komt hier. -->
			</ul>
		</li>
	</ul>'; }
	?>
</div>