<?php
require_once 'includes/globals.php';
require_once 'includes/requireSession.php';
require_once 'includes/requirePenningmeester.php';
require_once 'includes/functions.php';
require_once 'includes/connectdb.php';
?>
<!DOCTYPE html>
<html lang="nl">

<head>
    <?php

    include_once 'includes/head.php';

    ?>

    <title><?php echo SITE_TITLE; ?> - Kasboek </title>
</head>

<body>

<?php include_once 'includes/wrapper.php'; ?>

<!-- Sidebar -->
<?php

include_once 'includes/sidebar.php';

?>
<!-- /#sidebar-wrapper -->

<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h1>Kasboek <small>Voeg toe</small></h1>
                </div>
                <p>Op deze pagina kunt u gegevens in het kasboek zetten, deze worden direct opgeslagen wanneer u op volgende drukt</p>
                <p>Wanneer u meerdere kasboek gegevens wilt invoeren, kunt u kiezen voor "nog 1 toevoegen, wanneer u klaar bent kunt u weer op volgende drukken om verder te gaan</p>
                
                    <ul class="nav nav-tabs">
                        <li role="presentation"><a href="cashbook.php">Alle Kasboektransacties</a></li>
                        <li role="presentation"><a href="cashbook-add.php">Voeg toe aan Kasboek</a></li>
                        <li role="presentation" class="active"><a href="cashbookinvoeren.php">Invoeren geld</a></li>

                    </ul>
                    <?php
                    if ($_SERVER['REQUEST_METHOD'] == 'POST') {       				        		        	        
			            
			            $date = cleanInput($_POST['Datum']);
			            $amount = cleanInput($_POST['amount']);
			           
			            if(
			                validateInput($amount, 1, 32) //&&
			                //validateDate($date, 'Y-m-d')
			                ) 
			             {
			            		
                      	$data = array(                    	
                		'datum' => $date,
                		'bedrag' => $amount);
						
                      $insert = $dataManager->insert('oh_kasboek', $data);
					  if($insert) {
                    echo '<div class="alert alert-success" role="alert">Bedankt voor het aanvullen van de gegevens, ze zijn succesvol verwerkt!</div>';
                    echo '<p>Klik <a href="/webportal">hier</a> om naar de hoofdpagina te gaan.</p>';
                    echo "<p>Of klik <a href=".$_SERVER['REQUEST_URI'].">hier</a> om nog een bedrag toe te voegen.";
			                } else {
			                    echo '<div class="alert alert-danger" role="alert">Het lijkt er op alsof er een fout is met de verbinding van de database...</div>';
			                    echo "<p>Klik <a href=".$_SERVER['REQUEST_URI'].">hier</a> om het opnieuw te proberen.</p>";
			                }
			
			            } else {
			                echo '<div class="alert alert-danger" role="alert">Het lijkt er op alsof niet alle gegevens zijn ingevuld...</div>';
			                echo "<p>Klik <a href=".$_SERVER['REQUEST_URI'].">hier</a> om het opnieuw te proberen.</p>";
							print_r($amount);
			            }
			
			
			        } else {
                                     					 		         
                        ?>
                    <form class="clearfix horizontalSearchForm" id="addEntriesToCashBookForm" role="form" method="POST" enctype="multipart/form-data">
                    
                    <div class="col-md-14" align="left">
                        
                        <div align="left"  class="form-group col-md-10">
                        	<label for="amount">Bedrag:</label>
                        	<input type="text" class="form-control" name="amount" id="amount" number required data-progression="" data-helper="Vul hier uw bedrag in.">
                        </div>
                        <div class="col-md-2">
                        <div align="left"  class="form-group col-md-10">
                        	<label for="Datum">Datum:</label>
                        	<input type="text" class="form-control formDate" name="Datum" id="Datum" number required data-progression="" data-helper="Vul hier de datum in.">
                        </div>
                        <div class="col-md-2">
                       
                        <button type="submit" class="btn btn-primary " name="add" value="add" id="add">Opslaan</button>
                        
                       </form>
                       <?php 
                        }
                		?>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </body
</html>