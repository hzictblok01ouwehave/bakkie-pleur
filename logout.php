<?php
session_start();
?>
<!DOCTYPE html>
<html>
<body>
<h2>Je wordt nu uitgelogd</h2>
<?php
// remove all session variables
session_unset(); 

// destroy the session 
session_destroy();
header("location: index.php"); 
?>

</body>
</html> 